﻿using System;
using System.Linq;

namespace FizzBuzz
{
    class Program
    {
        public static bool keepAsking { get; set; } = true;
        static void Main(string[] args)
        {

            do
            {
                Console.WriteLine("Enter number between 1 and 100");
                FizzBuzz();
            } while (keepAsking);
        }
        public static void FizzBuzz()
        {
            string input = Console.ReadLine();
            if (int.TryParse(input, out int output))
            {
                int userInput = Convert.ToInt32(input);

                

                for (int i = 1; i < userInput + 1; i++)
                {
                    string returnString = "";
                    if (i % 3 == 0) returnString += "Fizz";
                    if (i % 5 == 0) returnString += "Buzz";
                    if (returnString.Length == 0)
                    {
                        Console.WriteLine(i);
                    }else Console.WriteLine(returnString);
                }

            } else keepAsking = true;

        }


    }
}

